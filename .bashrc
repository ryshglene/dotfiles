#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return
[[ -f ~/.shellrc ]] && . ~/.shellrc
unset HISTFILE
