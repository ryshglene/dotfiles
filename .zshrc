#! /usr/bin/env zsh

HISTSIZE=5000

bindkey -e
bindkey "^U"    backward-kill-line
bindkey "^[[H"  beginning-of-line
bindkey "^[[1~" beginning-of-line
bindkey "^[[F"  end-of-line
bindkey "^[[4~" end-of-line
bindkey "^[[5~" beginning-of-history
bindkey "^[[6~" end-of-history
bindkey "^[[3~" delete-char

zstyle ':completion:*' menu select=long-list
zstyle ':completion:*' matcher-list '' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' '+l:|=* r:|=*'

autoload -Uz compinit && compinit
setopt histignoredups completeinword correct globcomplete
unsetopt autoremoveslash autolist

autoload -Uz bracketed-paste-magic
zle -N bracketed-paste bracketed-paste-magic
autoload -Uz url-quote-magic
zle -N self-insert url-quote-magic

autoload -Uz colors && colors
[[ -f ~/.shellrc ]] && . ~/.shellrc
