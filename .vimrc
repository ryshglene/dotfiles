set expandtab
set shiftwidth=4
set softtabstop=4
set relativenumber
set number
set nowrap
set autoindent
set nostartofline
set foldmethod=marker

nnoremap <C-E> <Nop>
nnoremap <C-F> <Nop>
nnoremap <C-B> <Nop>

inoremap <C-E> <Nop>
inoremap <C-F> <Nop>
inoremap <C-B> <Nop>

vnoremap <C-E> <Nop>
vnoremap <C-F> <Nop>
vnoremap <C-B> <Nop>

cnoremap <C-F> <Nop>

nnoremap <silent> <PageUp> 1000<C-U>
nnoremap <silent> <PageDown> 1000<C-D>

inoremap <silent> <PageUp> <C-O>1000<C-U>
inoremap <silent> <PageDown> <C-O>1000<C-D>

vnoremap <silent> <PageUp> 1000<C-U>
vnoremap <silent> <PageDown> 1000<C-D>

colorscheme peachpuff
syntax on

if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif
