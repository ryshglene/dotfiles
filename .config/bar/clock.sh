#! /bin/sh

set -e

while date '+C %H:%M '; do
    sleep 60
done
