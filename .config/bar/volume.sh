#! /bin/sh

set -e

check_volstate() {
    local mixer_state="$(amixer get Master | sed -n 'N;s/^.*\[\([a-z]*\).*$/\1/p')"
    local mixer_level="$(amixer get Master | sed -n 'N;s/^.*\[\([0-9][0-9]*\).*$/\1/p')"
    [ "$mixer_state" = "on" ] && {
        case "$mixer_level" in
            [5-9][0-9]|1[0-4][0-9]|15[0-3])
                local volume_out="  "
                ;;
            [1-9]|[1-4][0-9])
                local volume_out="  "
                ;;
            0)
                local volume_out="  "
                ;;
        esac
    } || {
        local volume_out=""
    }
    if [ "$OUTPUT_LAST" != "$volume_out" ]; then
        OUTPUT_LAST="$volume_out"
        printf "V%s\n" "$volume_out"
    fi
}

check_volstate && while true; do
    stdbuf -oL alsactl monitor default | awk '/Playback Switch/ || /Playback Volume/ { print; fflush() }' | while read -r; do
        check_volstate
    done
done
