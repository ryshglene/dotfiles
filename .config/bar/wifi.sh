#! /bin/sh

set -e

devices=`ip link | sed -n 's/^[0-9]: \(.*\):.*$/\1/p'`
int1=$(echo $devices | awk '{print $2}')
int2=$(echo $devices | awk '{print $3}')

if echo $int1 | grep -oE "wl.*" > /dev/null; then
    wifi=$int1
else
    wifi=$int2
fi

if ip link show $wifi | grep -oq 'state UP'; then
    OUTPUT_CURR="  "
    OUTPUT_LAST="  "
fi

printf "W%s\n" "$OUTPUT_CURR"

nmcli device monitor 2> /dev/null | awk "/$wifi: connected/ || \$0 == \"$wifi: disconnected\" || \$0 == \"$wifi: unavailable\" && \$0!=l { print \$2; l=\$0; fflush() }" | while read -r line; do
    case "$line" in
        connected)                  OUTPUT_CURR="  " ;;
        disconnected|unavailable)   OUTPUT_CURR=""  ;;
    esac

    if [ "$OUTPUT_LAST" != "$OUTPUT_CURR" ]; then
        OUTPUT_LAST="$OUTPUT_CURR"
        printf "W%s\n" "$OUTPUT_CURR"
    fi
done
