#! /bin/sh

set -e

BAT_FULL=
BAT_TRFR=
BAT_HALF=
BAT_QRTR=
BAT_ZERO=
CHARGING=
AC_DEV="/sys/class/power_supply/AC/online"
BAT_DEV="/sys/class/power_supply/BAT0/capacity"

test ! -f "$BAT_DEV" && printf "B%s\n" " $CHARGING " && sleep inf

show_batlvl() {
    read -r AC_ISPLUGGED < $AC_DEV
    read -r BATTERY_CURR < $BAT_DEV

    if [ $AC_ISPLUGGED -eq 1 ]; then
        PLUGSTATE_CURR="AC"
        OUTPUT_CURR=" $CHARGING "
    else
        PLUGSTATE_CURR="BAT"
        case $BATTERY_CURR in
             [0-9]|1[0-9])      OUTPUT_CURR=" $BAT_ZERO " ;; #  0-19
            2[0-9]|3[0-9])      OUTPUT_CURR=" $BAT_QRTR " ;; # 20-39
            4[0-9]|5[0-9])      OUTPUT_CURR=" $BAT_HALF " ;; # 40-59
            6[0-9]|7[0-9])      OUTPUT_CURR=" $BAT_TRFR " ;; # 60-79
            *)                  OUTPUT_CURR=" $BAT_FULL " ;; # 80-100
        esac
    fi

    if pgrep X > /dev/null && [ ! -z "$DISPLAY" ]; then
        if [ "$BATTERY_CURR" -eq 100 ] && [ "$BATTERY_LAST" -lt "$BATTERY_CURR" ]; then
            mpv --really-quiet /usr/share/sounds/freedesktop/stereo/dialog-information.oga &
            notify-send -u low "Battery level is Full!" "Your battery level is at $BATTERY_CURR%"
        elif [ "$BATTERY_CURR" -le 10 ] && [ "$BATTERY_LAST" -gt "$BATTERY_CURR" ]; then
            mpv --really-quiet /usr/share/sounds/freedesktop/stereo/dialog-warning.oga &
            notify-send -u critical "Battery level is Low!" "Your battery level is at $BATTERY_CURR%"
        else
            if [ "$PLUGSTATE_LAST" = "BAT" ] && [ "$PLUGSTATE_CURR" = "AC" ]; then
                mpv --really-quiet /usr/share/sounds/freedesktop/stereo/power-plug.oga &
                notify-send -u normal -t 1500 "Battery charger plugged" "Your battery level is at $BATTERY_CURR%"
            elif [ "$PLUGSTATE_LAST" = "AC" ] && [ "$PLUGSTATE_CURR" = "BAT" ]; then
                mpv --really-quiet /usr/share/sounds/freedesktop/stereo/power-unplug.oga &
                notify-send -u normal -t 1500 "Battery charger removed" "Your battery level is at $BATTERY_CURR%"
            fi
        fi
    fi

    if [ "$PLUGSTATE_LAST" != "$PLUGSTATE_CURR" ]; then
        PLUGSTATE_LAST="$PLUGSTATE_CURR"
    fi
    if [ "$BATTERY_LAST" != "$BATTERY_CURR" ]; then
        BATTERY_LAST="$BATTERY_CURR"
    fi
    if [ "$OUTPUT_LAST" != "$OUTPUT_CURR" ]; then
        OUTPUT_LAST="$OUTPUT_CURR"
        printf "B%s\n" "$OUTPUT_CURR"
    fi
}

show_batlvl && upower --monitor | awk '/battery_BAT0/ { print $2 " " $3 "   " $4; fflush() }' | while read -r; do
    show_batlvl
done
