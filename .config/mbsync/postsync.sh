#! /bin/sh

MAILDIR=$HOME/.cache/maildir
NEWMAIL_SPOOL=$MAILDIR/temp/received
NEWMAIL_TALLY=0

test ! -e $NEWMAIL_SPOOL && touch $NEWMAIL_SPOOL

chmod 600 $NEWMAIL_SPOOL
for NEWMAIL in $(find $MAILDIR/data/INBOX/new -type f); do
    NEWMAIL_ID=$(basename $NEWMAIL)
    if ! grep -Fxq "$NEWMAIL_ID" $NEWMAIL_SPOOL; then
        echo "$NEWMAIL_ID" >> $NEWMAIL_SPOOL
        NEWMAIL_TALLY=$((NEWMAIL_TALLY+1))
    fi
done
chmod 400 $NEWMAIL_SPOOL

pgrep X > /dev/null && [ ! -z "$DISPLAY" ] || exit 0
if [ "$NEWMAIL_TALLY" -gt 0 ]; then
    mpv --really-quiet /usr/share/sounds/freedesktop/stereo/dialog-information.oga &
    notify-send -u normal "New Messages in Inbox" "Your mailbox has $NEWMAIL_TALLY new e-mail."
fi
