# This file is part of ranger, the console file manager.
# License: GNU GPL version 3, see the file "AUTHORS" for details.

from __future__ import (absolute_import, division, print_function)

from ranger.gui.colorscheme import ColorScheme
from ranger.gui.color import (
    black, blue, cyan, green, magenta, red, white, yellow, default,
    normal, bold, reverse, dim, BRIGHT,
    default_colors,
)


class TheLastNight(ColorScheme):
    progress_bar_color = white

    def use(self, context):  # pylint: disable=too-many-branches,too-many-statements
        fg, bg, attr = default_colors

        if context.reset:
            return default_colors

        elif context.in_browser:
            if context.selected:
                attr = reverse
            else:
                attr = normal
            if context.empty or context.error:
                fg = black
                bg = white
            if context.border:
                fg = default
            if context.media:
                if context.image or context.video or context.audio:
                    fg = yellow
                else:
                    fg = magenta
            if context.container:
                attr |= bold
                fg = red
            if context.directory:
                attr |= bold
                fg = white
            elif context.executable and not \
                    any((context.media, context.container,
                         context.fifo, context.socket)):
                attr |= bold
                fg = green
            if context.socket:
                attr |= bold
                fg = magenta
            if context.fifo or context.device:
                fg = magenta
                if context.device:
                    attr |= bold
            if context.link:
                fg = blue if context.good else red
            if context.tag_marker and not context.selected:
                attr |= bold
                if fg in (red, blue, yellow, green):
                    fg = cyan
                else:
                    fg = blue
            if not context.selected and (context.cut or context.copied):
                attr |= bold
                fg = cyan
            if context.selected and (context.cut or context.copied):
                attr |= bold
                fg = cyan
            if context.main_column:
                if context.selected:
                    attr |= bold
                if context.marked:
                    attr |= bold
                    fg = cyan
            if context.badinfo:
                if attr & reverse:
                    bg = magenta
                else:
                    fg = magenta

            if context.inactive_pane:
                fg = cyan

        elif context.in_titlebar:
            attr |= bold
            if context.hostname:
                fg = red if context.bad else cyan
            elif context.directory:
                fg = white
            elif context.tab:
                if context.good:
                    fg = black
                    bg = white
            elif context.link:
                fg = blue

        elif context.in_statusbar:
            if context.permissions:
                fg = white
            if context.marked:
                attr |= bold | reverse
                fg = yellow
            if context.frozen:
                attr |= bold | reverse
                fg = cyan
            if context.message:
                if context.bad:
                    attr |= bold
                    fg = red
            if context.loaded:
                bg = self.progress_bar_color
            if context.vcsinfo:
                fg = blue
                attr &= ~bold
            if context.vcscommit:
                fg = yellow
                attr &= ~bold
            if context.vcsdate:
                fg = cyan
                attr &= ~bold

        if context.text:
            if context.highlight:
                attr |= reverse

        if context.in_taskview:
            if context.title:
                fg = white

            if context.selected:
                attr |= reverse

            if context.loaded:
                if context.selected:
                    fg = self.progress_bar_color
                else:
                    bg = self.progress_bar_color

        if context.vcsfile and not context.selected:
            attr &= ~bold
            if context.vcsconflict:
                fg = magenta
            elif context.vcsuntracked:
                fg = cyan
            elif context.vcschanged:
                fg = red
            elif context.vcsunknown:
                fg = red
            elif context.vcsstaged:
                fg = green
            elif context.vcssync:
                fg = green
            elif context.vcsignored:
                fg = default

        elif context.vcsremote and not context.selected:
            attr &= ~bold
            if context.vcssync or context.vcsnone:
                fg = green
            elif context.vcsbehind:
                fg = red
            elif context.vcsahead:
                fg = blue
            elif context.vcsdiverged:
                fg = magenta
            elif context.vcsunknown:
                fg = red

        return fg, bg, attr
