#! /bin/sh

set -e

[ -z "$1" ] && echo "Usage: $(basename $0) [FILE|URL]..." && exit 1

URL_COUNT=0
INSTANCE=$(echo | awk 'srand() {print rand()}' | cut -d . -f 2)

gui_grablink() {
    notify-send -u normal -t 0 "Retrieving image..." "$1"
    if curl -so "$FILE_PATH" "$1"; then
        pkill dunst
        setsid dunst &
    else
        pkill dunst
        setsid dunst &
        exit 1
    fi
}

gui_dispimgs() {
    sxiv -g 640x480 -ab "$@" > /dev/null 2>&1
}

tty_grablink() {
    curl -s "$1" | convert - jpg:"$FILE_PATH"
}

tty_dispimgs() {
    while [ $# -gt 0 ]; do
        echo +------------------------------------------------------------+
        img2txt -f utf8 "$1" | sed 's/^/|/; s/$/|/'
        echo +------------------------------------------------------------+
        printf "==> Filename: %s\n" "$(basename $1)"
        shift
    done
}

trap 'rm -f $TMP_FILES' EXIT

if pgrep X > /dev/null && [ ! -z "$DISPLAY" ]; then
    GRABLINK_CMD='gui_grablink'
    DISPIMGS_CMD='gui_dispimgs'
else
    GRABLINK_CMD='tty_grablink'
    DISPIMGS_CMD='tty_dispimgs'
fi

while [ $# -gt 0 ]; do
    if echo "$1" | grep -Eq '(https?|ftp|file)://[-A-Za-z0-9\+&@#/%?=~_|!:,.;]*[-A-Za-z0-9\+&@#/%=~_|]'; then
        URL_COUNT=$((URL_COUNT+1))
        FILE_PATH="/tmp/$INSTANCE-$URL_COUNT.tmp"
        ! $GRABLINK_CMD "$1" && echo "$(basename $0): An error occurred while downloading an image" && continue || true
        if [ -z "$TMP_FILES" ]; then
            TMP_FILES="$FILE_PATH"
        else
            TMP_FILES="$TMP_FILES $FILE_PATH"
        fi
    else
        FILE_PATH="$1"
    fi
    if [ -z "$FILES_LIST" ]; then
        FILES_LIST="$FILE_PATH"
    else
        FILES_LIST="$FILES_LIST $FILE_PATH"
    fi
    shift
done

set -- $FILES_LIST
unset FILES_LIST
while [ $# -gt 0 ]; do
    if [ -f "$1" ]; then
        FILE_PATH="$1"
        if [ -z "$FILES_LIST" ]; then
            FILES_LIST="$FILE_PATH"
        else
            FILES_LIST="$FILES_LIST $FILE_PATH"
        fi
    fi
    shift
done

! $DISPIMGS_CMD $FILES_LIST && echo "$(basename $0): An error occurred while opening the images" && exit 1 || true
