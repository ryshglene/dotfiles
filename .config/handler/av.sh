#! /bin/sh

set -e

if [ -z "$1" ]; then
    cat << EOF
Usage: $(basename $0) [FILE|URL]...
EOF
else
    test -z "$DISPLAY" && PLAYER_CMD='mpv --really-quiet "$1"' || PLAYER_CMD='setsid mpv --really-quiet "$1" &'
    while [ $# -gt 0 ]; do
        ! eval "$PLAYER_CMD" && shift && continue || shift
    done
fi
