#! /bin/sh

set -e

if [ -z "$1" ]; then
    cat << EOF
Usage: $(basename $0) [FILE|URL]...
EOF
else
    while [ $# -gt 0 ]; do
        test -f "$1" && TR_TORRENT_NAME=$(transmission-show "$1" | sed 1q | cut -d ' ' -f 2-) || TR_TORRENT_NAME="$1"
        if transmission-remote --add "$1"; then
            setsid mpv --really-quiet /usr/share/sounds/freedesktop/stereo/dialog-information.oga &
            test -z "$DISPLAY" || NOTIFY_CMD='notify-send -u normal -t 1000 "Torrent Added" "$TR_TORRENT_NAME"'
        else
            setsid mpv --really-quiet /usr/share/sounds/freedesktop/stereo/dialog-warning.oga &
            test -z "$DISPLAY" || NOTIFY_CMD='notify-send -u critical -t 1000 "Torrent Error" "$TR_TORRENT_NAME"'
        fi > /dev/null
        eval "${NOTIFY_CMD:-true}"
        wait
        shift
    done
fi
