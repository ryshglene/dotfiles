#! /bin/sh

set -e

gui_env() {
    PLAYER_CMD='setsid mpv --really-quiet "$1" &'
    DEFAULT_CMD='setsid xdg-open "$1" &'
}

tty_env() {
    PLAYER_CMD='mpv --really-quiet "$1"'
    DEFAULT_CMD='xdg-open "$1"'
}

if [ -z "$1" ]; then
    cat << EOF
Usage: $(basename $0) [FILE|URL]...
EOF
else
    [ -z "$DISPLAY" ] && tty_env || gui_env
    while [ $# -gt 0 ]; do
        if echo "$1" | grep -qP 'http(?:s?):\/\/(?:www\.)?youtu(?:be\.com\/watch\?v=|\.be\/)([\w\-\_]*)(&(amp;)?[\w\?=]*)?'; then
            eval "$PLAYER_CMD" || true
        else
            eval "$DEFAULT_CMD" || true
        fi
        shift
    done
fi
