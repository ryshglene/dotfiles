#! /bin/sh

if pgrep X > /dev/null; then
    [ -z "$DISPLAY" ] && echo "$(basename $0): DISPLAY is not accessible" && exit 1
else
    echo "$(basename $0): No instance of X is running."
    exit 1
fi

for pid in $(pidof -x $0); do
    if [ $pid != $$ ]; then
        echo "$(basename $0): An instance of $(basename $0) is already running."    
        exit 0
    fi 
done

cleanup() {
    echo "$(basename $0): Terminating process group..."
    pgrep -x workspaces.sh > /dev/null && pkill -x workspaces.sh
    pgrep -x bar > /dev/null && pkill -x bar
}

trap 'trap - TERM; cleanup; kill 0' INT TERM QUIT EXIT

bspwm &

wait
