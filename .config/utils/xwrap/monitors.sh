#! /bin/sh

if pgrep X > /dev/null; then
    [ -z "$DISPLAY" ] && echo "$(basename $0): DISPLAY is not accessible" && exit 1
else
    echo "$(basename $0): No instance of X is running."
    exit 1
fi

set -e

trap 'kill 0' EXIT

while true; do
    inotifywait -qq /sys/class/drm/card0-{`echo $BSPWM_INTERNALMONITOR | sed 's/.\{1\}$/-&/'`,`echo $BSPWM_EXTERNALMONITOR | sed 's/.\{1\}$/-&/'`}/status
    if xrandr | grep -o "$BSPWM_EXTERNALMONITOR connected" > /dev/null; then
        mpv --really-quiet /usr/share/sounds/freedesktop/stereo/power-plug.oga &
        xrandr --output "$BSPWM_EXTERNALMONITOR" --right-of "$BSPWM_INTERNALMONITOR" --auto
        echo monitor_added
    else
        mpv --really-quiet /usr/share/sounds/freedesktop/stereo/power-unplug.oga &
        xrandr --output "$BSPWM_EXTERNALMONITOR" --off
        echo monitor_removed
    fi
    wait
done
