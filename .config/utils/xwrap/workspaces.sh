#! /bin/sh

if pgrep X > /dev/null; then
    [ -z "$DISPLAY" ] && echo "$(basename $0): DISPLAY is not accessible" && exit 1
else
    echo "$(basename $0): No instance of X is running."
    exit 1
fi

for pid in $(pidof -x $0); do
    if [ $pid != $$ ]; then
        echo "$(basename $0): An instance of $(basename $0) is already running."    
        exit 0
    fi 
done

set -e

trap 'kill 0' EXIT

$HOME/.config/utils/xwrap/monitors.sh | while read -r line; do
    if [ `bspc query --monitors | wc -l` -eq 2 ]; then
        case $line in
            monitor_added)
                set -- "$(bspc query --desktops | tail -n 2 | sed -n 1p)"
                bspc desktop "$1" -m "$BSPWM_EXTERNALMONITOR"
                bspc desktop Desktop -r
                ;;
            monitor_removed)
                set -- $(bspc query --desktops --monitor $BSPWM_EXTERNALMONITOR)
                INT_MONITORS_COUNT="$(bspc query --desktops --monitor $BSPWM_INTERNALMONITOR | wc -l)"
                EXT_MONITORS_COUNT="$(bspc query --desktops --monitor $BSPWM_EXTERNALMONITOR | wc -l)"
                bspc monitor "$BSPWM_EXTERNALMONITOR" -a Desktop
                while [ $# -gt 0 ]; do
                    bspc desktop "$1" -m "$BSPWM_INTERNALMONITOR"
                    shift
                done
                if [ $INT_MONITORS_COUNT -eq 1 ] && [ $EXT_MONITORS_COUNT -eq 2 ]; then
                    set -- "$(bspc query --desktops | head -n 1)"
                    bspc desktop "$1" -m "$BSPWM_EXTERNALMONITOR"
                    bspc desktop "$1" -m "$BSPWM_INTERNALMONITOR"
                fi
                bspc monitor "$BSPWM_EXTERNALMONITOR" -r
                ;;
        esac
        bgset -d
        bar --reset
    fi
done
