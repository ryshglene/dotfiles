#! /bin/sh

pgrep X > /dev/null && [ ! -z "$DISPLAY" ] || exit 0
pgrep i3lock > /dev/null && exit 0
pgrep rofi > /dev/null && pkill rofi

LOCK_CMD="i3lock-fancy -n -f Roboto-Mono -- maim"
MPC_LOCKFILE="/tmp/mpc_wasplaying.lck"

DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/$(id -u)/bus notify-send DUNST_COMMAND_PAUSE
trap 'kill %%' TERM INT # kill locker if we get killed
$LOCK_CMD &             # run locker as a child
wait                    # wait for locker to exit
DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/$(id -u)/bus notify-send DUNST_COMMAND_RESUME

test -f "$MPC_LOCKFILE" && rm "$MPC_LOCKFILE" && mpc -q play || true
