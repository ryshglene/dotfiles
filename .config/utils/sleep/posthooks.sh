#! /bin/sh

pgrep X > /dev/null && [ ! -z "$DISPLAY" ] || exit 0

xset q | grep -oq "Monitor is Off" && xset dpms force on || true
