#! /bin/sh

pgrep mpd > /dev/null || exit 0

MPC_STATUS="$(mpc status | grep -o playing)"
MPC_LOCKFILE="/tmp/mpc_wasplaying.lck"

if [ "$MPC_STATUS" = playing ]; then
    touch "$MPC_LOCKFILE" && mpc pause > /dev/null
fi
