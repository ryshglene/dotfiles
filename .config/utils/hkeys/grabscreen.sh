#! /bin/sh

if pgrep X > /dev/null; then
    [ -z "$DISPLAY" ] && echo "$(basename $0): DISPLAY is not accessible" && exit 1
else
    echo "$(basename $0): No instance of X is running."
    exit 1
fi

file=Screenshot-$(date +%Y-%m-%d-%H-%M-%S).png

case "$1" in
    --help|-h)      cat << EOF
Usage: $(basename $0) [options]

Options:
    -h, --help      Show this help menu.
    -s, --select    Enable selection mode.
EOF
                    exit 0
                    ;;
    --select|-s)    maim --select --hidecursor $HOME/Pictures/screenshots/$file
                    notify-send -u normal -t 1000 "Screenshot Taken" "Image stored in ~/Pictures as:\n$file"
                    mpv --really-quiet /usr/share/sounds/freedesktop/stereo/screen-capture.oga
                    ;;
    *)              mpv --really-quiet /usr/share/sounds/freedesktop/stereo/screen-capture.oga &
                    maim --hidecursor $HOME/Pictures/screenshots/$file &
                    notify-send -u normal -t 1000 "Screenshot Taken" "Image stored in ~/Pictures as:\n$file"
                    ;;
esac
