#! /bin/sh

if pgrep X > /dev/null; then
    [ -z "$DISPLAY" ] && echo "$(basename $0): DISPLAY is not accessible" && exit 1
else
    echo "$(basename $0): No instance of X is running."
    exit 1
fi

TPAD_STATUS="$(synclient -l | grep TouchpadOff | awk '{print $3}')"

if [ "$TPAD_STATUS" = 0 ]; then
    synclient TouchpadOff=1
    echo "Touchpad is now disabled."
elif [ "$TPAD_STATUS" = 1 ]; then
    synclient TouchpadOff=0
    echo "Touchpad is now enabled."
fi
