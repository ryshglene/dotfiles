#! /bin/sh

if pgrep X > /dev/null; then
    [ -z "$DISPLAY" ] && echo "$(basename $0): DISPLAY is not accessible" && exit 1
else
    echo "$(basename $0): No instance of X is running."
    exit 1
fi

INT_MONITORS="$(bspc query --desktops --monitor $BSPWM_INTERNALMONITOR | grep ^0x | wc -l)"
EXT_MONITORS="$(bspc query --desktops --monitor $BSPWM_EXTERNALMONITOR | grep ^0x | wc -l)"

if [ $EXT_MONITORS -eq 0 ]; then
    exit 0
else
    if [ $INT_MONITORS -eq 2 ] && [ $EXT_MONITORS -eq 1 ]; then
        set -- "$(bspc query --desktops | sed -n 1p)" "$(bspc query --desktops | sed -n 2p)" "$(bspc query --desktops | sed -n 3p)"
        bspc desktop "$1" -m "$BSPWM_EXTERNALMONITOR"
        bspc desktop "$3" -m "$BSPWM_INTERNALMONITOR"
        bspc desktop "$2" -m "$BSPWM_EXTERNALMONITOR"
    elif [ $INT_MONITORS -eq 1 ] && [ $EXT_MONITORS -eq 2 ]; then
        set -- "$(bspc query --desktops | sed -n 1p)" "$(bspc query --desktops | sed -n 2p)" "$(bspc query --desktops | sed -n 3p)"
        bspc desktop "$2" -m "$BSPWM_INTERNALMONITOR"
        bspc desktop "$1" -m "$BSPWM_EXTERNALMONITOR"
        bspc desktop "$3" -m "$BSPWM_INTERNALMONITOR"
    else
        exit 0
    fi
fi
