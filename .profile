#! /bin/sh

EDITOR=vim
PAGER=less
PAGER=alacritty
VDPAU_DRIVER=va_gl
QT_QPA_PLATFORMTHEME=qt5ct
BSPWM_SOCKET=/tmp/bsp.sock
BSPWM_BOXPADDING_HORZ=24
BSPWM_BOXPADDING_VERT=12
BSPWM_BOXGAP=8
BSPWM_BWIDTH=6
BSPWM_INTERNALMONITOR=LVDS1
BSPWM_EXTERNALMONITOR=VGA1
BSPWM_WORKSPACES='I II III'
BG_IMAGE=$HOME/.config/bspwm/lakeside.jpg
LIBVA_DRIVER_NAME=i965
export  EDITOR PAGER\
        PANEL_HEIGHT PANEL_WIDTH PANEL_FONT\
        VDPAU_DRIVER\
        QT_QPA_PLATFORMTHEME\
        BSPWM_SOCKET\
            BSPWM_BOXPADDING_HORZ BSPWM_BOXPADDING_VERT\
        BSPWM_BOXGAP BSPWM_BWIDTH\
            BSPWM_INTERNALMONITOR BSPWM_EXTERNALMONITOR\
            BSPWM_WORKSPACES\
        BG_IMAGE\
        LIBVA_DRIVER_NAME

if [ -d $HOME/bin ]; then
    PATH=$HOME/bin:$PATH
    export PATH
fi
if [ -d /usr/lib/jvm/default ]; then
    PYCHARM_JDK=/usr/lib/jvm/default
    export PYCHARM_JDK
fi
if [ ! -f $HOME/.hushlogin ]; then
    echo ''
    echo '  >>>> Running post-installation script...'
    touch $HOME/.hushlogin
    $HOME/.config/install.d/post.sh
fi
